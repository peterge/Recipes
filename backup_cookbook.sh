#!/bin/bash

# Go to dir
cd /mnt/docker/nextcloud/data/peterge/files/Recipes

# Commit changes
git add .
git commit -m "Commiting recipes from $(date "+%y-%m-%d-%H:%M")"
git push
