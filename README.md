# How this works

I have set up the [Nextcloud Cookbook App](https://apps.nextcloud.com/apps/cookbook) on my local Nextcloud Server, which is running on my TrueNas SCALE Server. This Repo contains all data from the /Recipes directory, which is specified in the Cookbook app as default directory. 
Then a Cron Job is run monthly to execute the [backup_cookbook.sh](backup_cookbook.sh) Script.
The Cron Job looks like this:

```
Description
Nextcloud Cookbook
Command
cd /mnt/main/nextcloud/data/peterge/files/Recipes && bash backup_cookbook.sh
Run as user
root
Schedule
Monthly
```
